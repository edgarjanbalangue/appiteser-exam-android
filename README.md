# Appetiser Exam Android

This is a simple master-detail app on which it will fetch movies in itunes API.

  - MainActivity on which it will show all the movies
  - DetailActivity will show a detailed view of selected movie.

# Persistence

  - Since its only saving a small piece of data like (screen_name, id, date) I decided to user SharedPreferences on local caching. 
  - When I need to store more like big data or movie object for example I will use an actual database like RealM

# Architecture

 - I used MVP Architecture because it was cleaner than MVC. 
 
# Used cocoapods:
 - [ButterKnife](http://jakewharton.github.io/butterknife/)
 - [Retrofit](https://github.com/square/retrofit)
 - [Gson](https://github.com/google/gson)
 - [Picasso](https://github.com/square/picasso)
