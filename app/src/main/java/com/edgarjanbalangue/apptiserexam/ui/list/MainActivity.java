package com.edgarjanbalangue.apptiserexam.ui.list;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.edgarjanbalangue.apptiserexam.R;
import com.edgarjanbalangue.apptiserexam.data.DataManager;
import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.edgarjanbalangue.apptiserexam.data.track.source.TracksRepository;
import com.edgarjanbalangue.apptiserexam.ui.base.BaseActivity;
import com.edgarjanbalangue.apptiserexam.ui.detail.DetailActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity<MainPresenter> implements MainView, MovieAdapter.MovieListener {

    private MovieAdapter movieAdapter;
    private boolean alreadyLoaded = false;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.lastVisitedTv)
    TextView lastVisitedTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_list);
        ButterKnife.bind(this);
        movieAdapter = new MovieAdapter(this);
        recyclerView.setAdapter(movieAdapter);
        presenter.onAttach();

        presenter.checkLastDate(this);
        presenter.saveLastDate(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (alreadyLoaded) {
            presenter.saveScreen(this,this.getLocalClassName());
        }

    }

    @NonNull
    @Override
    protected MainPresenter createPresenter() {
        TracksRepository movieRepository = DataManager.getInstance().getMovieRepository();
        return new MainPresenter(this, movieRepository);
    }

    //region MainView Interface
    @Override
    public void showMovies(List<TrackModel> trackModels) {
        alreadyLoaded = true;
        movieAdapter.setItems(trackModels);
        presenter.checkLastScreen(this, trackModels);
    }

    @Override
    public void showErrorMessage() {
        Toast.makeText(this, "Server error!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showThereIsNoMovies() {
        Toast.makeText(this, "There is no movies!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLastDateVisited(String lastDate) {
        lastVisitedTv.setVisibility(View.VISIBLE);
        lastVisitedTv.setText("Last Visited : " + lastDate);
    }

    @Override
    public void goToDetailScreen(TrackModel trackModel) {
        DetailActivity.start(this, trackModel);
    }

    //endregion

    //region Adapter Listener
    @Override
    public void onMovieClicked(TrackModel trackModel) {
        DetailActivity.start(this, trackModel);
    }

    //endregion
}