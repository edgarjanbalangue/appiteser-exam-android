package com.edgarjanbalangue.apptiserexam.ui.detail;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;

import com.edgarjanbalangue.apptiserexam.R;
import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.edgarjanbalangue.apptiserexam.ui.base.BaseActivity;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends BaseActivity<DetailPresenter> implements DetailView {

    private static final String EXTRA_MOVIE = "EXTRA_MOVIE";

    public static void start(Context context, TrackModel trackModel) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(EXTRA_MOVIE, trackModel);
        context.startActivity(intent);
    }

    @BindView(R.id.artworkIV)
    AppCompatImageView artworkIV;
    @BindView(R.id.nameTv)
    TextView nameTv;
    @BindView(R.id.ratingTv)
    TextView ratingTv;
    @BindView(R.id.genreTv)
    TextView genreTv;
    @BindView(R.id.priceBtn)
    Button priceBtn;
    @BindView(R.id.longDescriptionTv)
    TextView longDescriptionTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        presenter.onAttach();
        presenter.saveTrackId(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.saveScreen(this,this.getLocalClassName());
    }

    @NonNull
    @Override
    protected DetailPresenter createPresenter() {
        TrackModel trackModel = getIntent().getParcelableExtra(EXTRA_MOVIE);
        return new DetailPresenter(this, trackModel);
    }

    //region DetailView Interface

    @Override
    public void showMovieData(TrackModel trackModel) {
        Picasso.get().load(trackModel.getArtworkURL()).placeholder(R.mipmap.place_holder).into(artworkIV);
        nameTv.setText(trackModel.getName());
        ratingTv.setText(trackModel.getRating());
        genreTv.setText(trackModel.getGenre());
        priceBtn.setText("$" + String.valueOf(trackModel.getPrice()));
        longDescriptionTv.setText(trackModel.getLongDescription());
    }

    @Override
    public void showDataUnavailableMessage() {
        Toast.makeText(this, "Data Unavailable", Toast.LENGTH_SHORT).show();
    }

    //endregion

}
