package com.edgarjanbalangue.apptiserexam.ui.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.edgarjanbalangue.apptiserexam.R;
import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder>{

    public interface MovieListener {
        void onMovieClicked(TrackModel trackModel);
    }

    private List<TrackModel> items;
    private MovieListener listener;

    public MovieAdapter(MovieListener listener) {
        this.listener = listener;
        items = new ArrayList<>();
    }

    public void setItems(List<TrackModel> items) {
        this.items = items;
        this.notifyDataSetChanged();
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_movie, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private TrackModel getItem(int position) {
        return items.get(position);
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.artworkIV) AppCompatImageView artworkIV;
        @BindView(R.id.nameTv) TextView nameTv;
        @BindView(R.id.genreTv) TextView genreTv;
        @BindView(R.id.priceTv) TextView priceTv;

        MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(int position) {
            TrackModel trackModel = getItem(position);

            setClickListener(trackModel);
            setImage(trackModel.getArtworkURL());
            setName(trackModel.getName());
            setGenre(trackModel.getGenre());
            setPrice("$" + String.valueOf(trackModel.getPrice()));
        }

        private void setName(String title) {
            this.nameTv.setText(title);
        }

        private void setImage(String imageUrl) {
            Picasso.get().load(imageUrl).placeholder(R.mipmap.place_holder).into(artworkIV);
        }

        private void setGenre(String genre) {
            this.genreTv.setText(genre);
        }

        private void setPrice(String price) {
            this.priceTv.setText(price);
        }

        private void setClickListener(TrackModel trackModel) {
            itemView.setTag(trackModel);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onMovieClicked((TrackModel) view.getTag());
        }
    }
}
