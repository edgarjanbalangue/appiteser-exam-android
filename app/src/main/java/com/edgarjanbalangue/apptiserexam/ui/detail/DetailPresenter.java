package com.edgarjanbalangue.apptiserexam.ui.detail;

import android.content.Context;

import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.edgarjanbalangue.apptiserexam.data.track.source.local.AppLocalApi;
import com.edgarjanbalangue.apptiserexam.ui.base.BasePresenter;

public class DetailPresenter extends BasePresenter<DetailView> {

    private final TrackModel trackModel;

    DetailPresenter(DetailView view, TrackModel trackModel) {
        super(view);
        this.trackModel = trackModel;
    }

    public void onAttach() {
        showMovieData();
    }

    public void saveScreen(Context context, String className) {
        AppLocalApi.saveUserLastScreen(context,className);
    }

    public void saveTrackId(Context context) {
        AppLocalApi.saveTrackId(context, trackModel.getId());
    }

    private void showMovieData() {
        if (trackModel != null) {
            view.showMovieData(trackModel);
        }
    }
}
