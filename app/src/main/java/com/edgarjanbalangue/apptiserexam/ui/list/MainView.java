package com.edgarjanbalangue.apptiserexam.ui.list;

import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.edgarjanbalangue.apptiserexam.ui.base.BaseView;

import java.util.List;

public interface MainView extends BaseView {

    void showMovies(List<TrackModel> trackModels);

    void showErrorMessage();
    void showLastDateVisited(String lastDate);

    void showThereIsNoMovies();

    void goToDetailScreen(TrackModel trackModel);
}
