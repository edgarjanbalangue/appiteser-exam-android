package com.edgarjanbalangue.apptiserexam.ui.list;

import android.content.Context;

import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.edgarjanbalangue.apptiserexam.data.track.source.TrackDataSource;
import com.edgarjanbalangue.apptiserexam.data.track.source.TracksRepository;
import com.edgarjanbalangue.apptiserexam.data.track.source.local.AppLocalApi;
import com.edgarjanbalangue.apptiserexam.ui.base.BasePresenter;

import java.lang.ref.WeakReference;
import java.util.List;

public class MainPresenter extends BasePresenter<MainView> {

    private final TracksRepository movieRepository;

    MainPresenter(MainView view, TracksRepository movieRepository) {
        super(view);
        this.movieRepository = movieRepository;
    }

    public void onAttach() {
        getAllMovies();
    }

    public void saveScreen(Context context, String className) {
        AppLocalApi.saveUserLastScreen(context,className);
    }

    public void saveLastDate(Context context) {
        AppLocalApi.saveUserLastVisitDateTime(context);
    }

    public void checkLastDate(Context context) {
        String lastVisitedDate = AppLocalApi.getUserLastVisitDateTime(context);
        if (lastVisitedDate != null) {
            view.showLastDateVisited(lastVisitedDate);
        }
    }

    /**
     * Network
     **/
    private void getAllMovies() {
        movieRepository.getTracks(new MovieCallListener(view));
    }

    /**
     * Callback
     **/
    private static class MovieCallListener implements TrackDataSource.LoadMoviesCallback {

        private WeakReference<MainView> view;

        private MovieCallListener(MainView view) {
            this.view = new WeakReference<>(view);
        }

        @Override
        public void onTracksLoaded(List<TrackModel> trackModels) {
            if (view.get() == null) return;
            view.get().showMovies(trackModels);
        }

        @Override
        public void onDataNotAvailable() {
            if (view.get() == null) return;
            view.get().showThereIsNoMovies();

        }

        @Override
        public void onError() {
            if (view.get() == null) return;
            view.get().showErrorMessage();

        }
    }

    public void checkLastScreen(Context context,List<TrackModel> trackModels) {
        String lastScreen = AppLocalApi.getUserLastScreen(context);
        if (lastScreen != null) {
            switch (lastScreen) {
                case "ui.detail.DetailActivity":
                    final int trackID = AppLocalApi.getTrackId(context);
                    TrackModel trackModel1 = null;
                    for(TrackModel trackModel : trackModels) {
                        if(trackModel.getId() == trackID) {
                            trackModel1 = trackModel;
                            break;
                        }
                    }
                    view.goToDetailScreen(trackModel1);
                    break;
            }
        }
    }

}
