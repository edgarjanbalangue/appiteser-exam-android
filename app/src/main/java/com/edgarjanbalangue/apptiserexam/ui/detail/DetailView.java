package com.edgarjanbalangue.apptiserexam.ui.detail;

import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.edgarjanbalangue.apptiserexam.ui.base.BaseView;

public interface DetailView extends BaseView {

    void showMovieData(TrackModel trackModel);

    void showDataUnavailableMessage();
}
