package com.edgarjanbalangue.apptiserexam.data.track.source.remote.service;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SearchService {

    private static final String URL = "https://itunes.apple.com";

    private AppRemoteApi mAppRemoteApi;

    private static SearchService singleton;

    private SearchService() {
        Retrofit mRetrofit = new Retrofit.Builder().addConverterFactory(GsonConverterFactory.create()).baseUrl(URL).build();
        mAppRemoteApi = mRetrofit.create(AppRemoteApi.class);
    }

    public static SearchService getInstance() {
        if (singleton == null) {
            singleton = new SearchService();
        }
        return singleton;
    }

    public AppRemoteApi getTrackApi() {
        return mAppRemoteApi;
    }
}
