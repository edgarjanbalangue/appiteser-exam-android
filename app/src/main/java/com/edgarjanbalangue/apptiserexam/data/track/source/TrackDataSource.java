package com.edgarjanbalangue.apptiserexam.data.track.source;

import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;

import java.util.List;

public interface TrackDataSource {

    interface LoadMoviesCallback {
        void onTracksLoaded(List<TrackModel> trackModels);
        void onDataNotAvailable();
        void onError();
    }

    void getTracks(LoadMoviesCallback callback);

}
