package com.edgarjanbalangue.apptiserexam.data.track.source.remote;

import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.edgarjanbalangue.apptiserexam.data.track.source.TrackDataSource;
import com.edgarjanbalangue.apptiserexam.data.track.source.remote.model.TrackResponse;
import com.edgarjanbalangue.apptiserexam.data.track.source.remote.service.AppRemoteApi;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TrackRemoteDataSource implements TrackDataSource {

    private static TrackRemoteDataSource instance;

    private final AppRemoteApi appRemoteApi;

    private TrackRemoteDataSource(AppRemoteApi appRemoteApi) {
        this.appRemoteApi = appRemoteApi;
    }

    public static TrackRemoteDataSource getInstance(AppRemoteApi appRemoteApi) {
        if (instance == null) {
            instance = new TrackRemoteDataSource(appRemoteApi);
        }
        return instance;
    }

    @Override
    public void getTracks(final LoadMoviesCallback callback) {
        appRemoteApi.getTracks().enqueue(new Callback<TrackResponse>() {
            @Override
            public void onResponse(Call<TrackResponse> call, Response<TrackResponse> response) {
                List<TrackModel> trackModels = response.body() != null ? response.body().getMovies() : null;
                if (trackModels != null && !trackModels.isEmpty()) {
                    callback.onTracksLoaded(trackModels);
                } else {
                    callback.onDataNotAvailable();
                }
            }

            @Override
            public void onFailure(Call<TrackResponse> call, Throwable t) {
                callback.onError();
            }
        });
    }


}
