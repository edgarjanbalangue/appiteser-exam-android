package com.edgarjanbalangue.apptiserexam.data.track.source;

import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.edgarjanbalangue.apptiserexam.data.track.source.remote.TrackRemoteDataSource;

import java.util.List;

public class TracksRepository implements TrackDataSource {

    private final TrackDataSource trackRemote;

    private static TracksRepository instance;

    public TracksRepository(TrackRemoteDataSource trackRemote) {
        this.trackRemote = trackRemote;
    }

    public static TracksRepository getInstance(TrackRemoteDataSource movieRemote
                                               ) {
        if (instance == null) {
            instance = new TracksRepository(movieRemote);
        }
        return instance;
    }

    @Override
    public void getTracks(LoadMoviesCallback callback) {

        if (callback == null) return;

        getMoviesFromRemoteDataSource(callback);
    }

    private void getMoviesFromRemoteDataSource(final LoadMoviesCallback callback) {
        trackRemote.getTracks(new LoadMoviesCallback() {
            @Override
            public void onTracksLoaded(List<TrackModel> trackModels) {
                callback.onTracksLoaded(trackModels);
            }

            @Override
            public void onDataNotAvailable() {
                callback.onDataNotAvailable();
            }

            @Override
            public void onError() {
                callback.onError();
            }
        });
    }
}
