package com.edgarjanbalangue.apptiserexam.data.track.source.local;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.SimpleDateFormat;
import java.util.Date;

import static android.content.Context.MODE_PRIVATE;

public class AppLocalApi {

    private static final String PREFERENCE_NAME = "AppetiserExam.Preference";
    private static final String LAST_DATE_VISITED = "AppetiserExam.LastDateVisited";
    private static final String LAST_SCREEN = "AppetiserExam.LastScreen";
    private static final String MOVIE_ID = "AppetiserExam.TrackId";

    public static void saveUserLastVisitDateTime(Context context) {
        String date = new SimpleDateFormat("MMMM dd,yyyy h:mm a").format(new Date());
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putString(LAST_DATE_VISITED, date);
        editor.apply();
    }

    public static String getUserLastVisitDateTime(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE);
        return prefs.getString(LAST_DATE_VISITED,null);
    }

    public static void saveUserLastScreen(Context context, String screenName) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putString(LAST_SCREEN, screenName);
        editor.apply();
    }

    public static String getUserLastScreen(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE);
        return prefs.getString(LAST_SCREEN,null);
    }

    public static int getTrackId(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE);
        return prefs.getInt(MOVIE_ID,-1);
    }

    public static void saveTrackId(Context context, int trackId) {
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFERENCE_NAME, MODE_PRIVATE).edit();
        editor.putInt(MOVIE_ID, trackId);
        editor.apply();
    }


}
