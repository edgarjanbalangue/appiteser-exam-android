package com.edgarjanbalangue.apptiserexam.data.track.source.remote.service;

import com.edgarjanbalangue.apptiserexam.data.track.source.remote.model.TrackResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface AppRemoteApi {
    @GET("/search?term=star&country=au&media=movie&all")
    Call<TrackResponse> getTracks();
}
