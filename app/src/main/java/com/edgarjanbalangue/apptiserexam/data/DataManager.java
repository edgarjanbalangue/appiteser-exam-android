package com.edgarjanbalangue.apptiserexam.data;

import com.edgarjanbalangue.apptiserexam.data.track.source.TracksRepository;
import com.edgarjanbalangue.apptiserexam.data.track.source.remote.TrackRemoteDataSource;
import com.edgarjanbalangue.apptiserexam.data.track.source.remote.service.AppRemoteApi;
import com.edgarjanbalangue.apptiserexam.data.track.source.remote.service.SearchService;

public class DataManager {
    private static DataManager sInstance;

    private DataManager() {
        // This class is not publicly instantiable
    }

    public static synchronized DataManager getInstance() {
        if (sInstance == null) {
            sInstance = new DataManager();
        }
        return sInstance;
    }


    public TracksRepository getMovieRepository() {

        AppRemoteApi appRemoteApi = SearchService.getInstance().getTrackApi();
        TrackRemoteDataSource trackRemote = TrackRemoteDataSource.getInstance(appRemoteApi);

        return TracksRepository.getInstance(trackRemote);
    }
}
