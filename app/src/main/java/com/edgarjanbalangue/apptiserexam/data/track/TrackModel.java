package com.edgarjanbalangue.apptiserexam.data.track;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class TrackModel implements Parcelable {

    @SerializedName("trackId")
    private int id;

    @SerializedName("trackName")
    private String name;

    @SerializedName("trackPrice")
    private double price;

    @SerializedName("primaryGenreName")
    private String genre;

    @SerializedName("artworkUrl100")
    private String artworkURL;

    @SerializedName("longDescription")
    private String longDescription;

    @SerializedName("kind")
    private String kind;

    @SerializedName("contentAdvisoryRating")
    private String rating;

    public TrackModel() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getArtworkURL() {
        return artworkURL;
    }

    public void setArtworkURL(String artworkURL) {
        this.artworkURL = artworkURL;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    protected TrackModel(Parcel in) {
        id = in.readInt();
        name = in.readString();
        price = in.readDouble();
        genre = in.readString();
        artworkURL = in.readString();
        longDescription = in.readString();
        kind = in.readString();
        rating = in.readString();
    }

    public static final Creator<TrackModel> CREATOR = new Creator<TrackModel>() {
        @Override
        public TrackModel createFromParcel(Parcel in) {
            return new TrackModel(in);
        }

        @Override
        public TrackModel[] newArray(int size) {
            return new TrackModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeDouble(price);
        parcel.writeString(genre);
        parcel.writeString(artworkURL);
        parcel.writeString(longDescription);
        parcel.writeString(kind);
        parcel.writeString(rating);
    }
}
