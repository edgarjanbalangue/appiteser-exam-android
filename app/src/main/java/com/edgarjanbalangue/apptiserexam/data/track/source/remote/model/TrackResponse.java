package com.edgarjanbalangue.apptiserexam.data.track.source.remote.model;

import com.edgarjanbalangue.apptiserexam.data.track.TrackModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TrackResponse {
    @SerializedName("results")
    private List<TrackModel> trackModels;

    public List<TrackModel> getMovies() {
        return trackModels;
    }

    public void setMovies(List<TrackModel> trackModels) {
        this.trackModels = trackModels;
    }
}
